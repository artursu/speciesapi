package speciesAPI

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

var Uptime time.Time //Creates a global time variable that is then used by main to retrieve time

//Exclude duplicates from Occurences
func excludeDuplicates(source occurrences, destination *occurrences) {

	isInArray := false
	//Goes through every Occurrence in source
	for i := 0; i < len(source.Results); i++ {
		isInArray = false
		//Checks if it already is in the destination
		for j := 0; j < len(destination.Results); j++ {
			if source.Results[i] == destination.Results[j] {
				isInArray = true
			}
		}
		//If not, adds it to the destination
		if !isInArray {
			destination.Results = append(destination.Results, source.Results[i])
		}
	}
}

//Separates the array retrieved from GBIF to match required output format
func divide(source occurrences, destination *countryAndSpecies) {
	//Goes through ever Occurrence in the source
	for i := 0; i < len(source.Results); i++ {
		//Adds data to respective arrays
		destination.Species = append(destination.Species, source.Results[i].ScientificName)
		//Checks if speciesKey is present, and if not - uses acceptedTaxonKey instead
		if source.Results[i].SpeciesKey != 0 {
			destination.SpeciesKey = append(destination.SpeciesKey, source.Results[i].SpeciesKey)
		} else {
			destination.SpeciesKey = append(destination.SpeciesKey, source.Results[i].AcceptedTaxonKey)
		}
	}
}

//Handles species endpoint
func HandlerSpecies(w http.ResponseWriter, r *http.Request) {

	//Initializing structs to receive data from GBIF
	var message species
	var messageYear species

	//Splits the URL to retrieve speciesKey from client
	parts := strings.Split(r.URL.Path, "/")
	//Checks request formatting and whether speciesKey is present
	if len(parts) != 5 || parts[4] == ""{
		http.Error(w, "Expecting format .../specieskey", http.StatusBadRequest)
		return
	}

	//Requests species data from GBIF
	res, err := http.Get("http://api.gbif.org/v1/species/" + parts[4])

	//Checks for errors
	if err != nil {
		log.Print(err)
		//If request fails - throw 500
		http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
		return
	}

	//closes response body when the function returns
	defer res.Body.Close()

	//Decodes received data into struct
	err = json.NewDecoder(res.Body).Decode(&message)

	//Checks for errors
	if err != nil {
		log.Print(err)
		//Sends Not Found status if the decoding fails
		http.Error(w, "404 Not Found", http.StatusNotFound)
		return
	}

	//Requests data about species using a different GBIF endpoint
	//Is used only to retrieve "year"
	res, err = http.Get("http://api.gbif.org/v1/species/" + parts[4] + "/name")

	//Checks for errors
	if err != nil {
		log.Print(err)
		//If request fails - throw 500
		http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
		return
	}

	//Gets year
	err = json.NewDecoder(res.Body).Decode(&messageYear)

	//Checks for errors
	if err != nil {
		log.Print(err)
		//Sends Not Found status if the decoding fails
		http.Error(w, "404 Not Found", http.StatusNotFound)
		return
	}

	//Assigns year
	message.Year = messageYear.Year

	//Initializes a binary variable to decode into
	var buffer = new(bytes.Buffer)

	//Encodes into binary
	encoder := json.NewEncoder(buffer)
	encoder.Encode(message)

	//Sets headers
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	//Streams binary into ResponseWriter
	io.Copy(w, buffer)
}

//Handles country endpoint
func HandlerOccurrence(w http.ResponseWriter, r *http.Request) {

	//Initializes string to store URL
	var stringAddr string

	//Initializes variables to store received data from GBIF and RESTcountries
	var received occurrences
	var message occurrences
	var message2 country

	//Initializes a new struct to combine the data
	var finalMessage countryAndSpecies

	//Gets limit from the client
	limit := r.FormValue("limit")

	//Splits the URL to retrieve country code
	parts := strings.Split(r.URL.Path, "/")
	//Checks request formatting and whether country code is present
	if len(parts) != 5 || parts[4] == "" {
		status := http.StatusBadRequest
		http.Error(w, "Expecting format .../countrycode", status)
		return
	}

	//Checks if limit is specified, and then combines address into string
	if limit != "" {
		stringAddr = "http://api.gbif.org/v1/occurrence/search?country=" + parts[4] + "&limit=" + limit
	} else {
		stringAddr = "http://api.gbif.org/v1/occurrence/search?country=" + parts[4]
	}

	//Requests occurrence data from GBIF
	res, err := http.Get(stringAddr)

	//Checks for errors
	if err != nil {
		log.Print(err)
		//If request fails - throw 500
		http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = json.NewDecoder(res.Body).Decode(&received) // Decodes the information

	//Checks for errors
	if err != nil {
		log.Print(err)
		//Sends Not Found status if the decoding fails
		http.Error(w, "404 Not Found", http.StatusNotFound)
		return
	}

	//Excludes duplicates from Occurences
	excludeDuplicates(received, &message)

	//Closes body
	res.Body.Close()

	//Requests country data from RESTcountries
	res, err = http.Get("http://restcountries.eu/rest/v2/alpha/" + parts[4])

	//Checks for errors
	if err != nil {
		log.Print(err)
		//If request fails - throw 500
		http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
		return
	}

	//Closes body when function returns
	defer res.Body.Close()

	//Decodes received data into struct
	err = json.NewDecoder(res.Body).Decode(&message2)

	//Checks for errors
	if err != nil {
		log.Print(err)
		//Sends Not Found status if the decoding fails
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	//Combines the data from two sources
	finalMessage.Name = message2.Name
	finalMessage.Capital = message2.Capital
	finalMessage.Flag = message2.Flag
	divide(message, &finalMessage)

	//Initializes a binary variable to decode into
	var buffer = new(bytes.Buffer)

	//Encodes into binary
	encoder := json.NewEncoder(buffer)
	encoder.Encode(finalMessage)

	//Writes headers
	w.Header().Add("Content-Type", "application/json") // Displays the mesage in json formating
	w.WriteHeader(http.StatusOK)

	//Streams binary into ResponseWriter
	io.Copy(w, buffer)

}

//Handles diag endpoint
func HandlerDiag(w http.ResponseWriter, r *http.Request) {

	//Initializes a struct to store diagnostics data
	var diag diagStruct

	//Retrieves a random (but not really) species to check if the service is online
	res, err := http.Get("http://api.gbif.org/v1/species/5787161")

	//Checks for errors
	if err != nil {
		log.Print(err)
		//If request fails, sets status to 500
		diag.Gbif = string(http.StatusInternalServerError)
		return
	}

	//Sets status code retrieved from GBIF
	diag.Gbif = res.Status

	//Retrieves a random (but not really) country to check if the service is online
	res, err = http.Get("http://restcountries.eu/rest/v2/alpha/LV")

	//Checks for errors
	if err != nil {
		log.Print(err)
		//If request fails, sets status to 500
		diag.Restcountries = string(http.StatusInternalServerError)
		return
	}

	//Sets status code retrieved from RESTcountries
	diag.Restcountries = res.Status

	//Closes body when function returns
	defer res.Body.Close()

	//Sets current version and uptime
	diag.Version = "v1"
	diag.Uptime = time.Since(Uptime).String()

	//Initializes a binary variable to encode into
	var buffer = new(bytes.Buffer)

	//Encodes into binary
	encoder := json.NewEncoder(buffer)
	encoder.Encode(diag)

	//Writes headers
	w.Header().Add("Content-Type", "application/json") //json formating
	w.WriteHeader(http.StatusOK)

	//Streams binary into ResponseWriter
	io.Copy(w, buffer)

}

//Handles incorrect request formats
func HandlerBadRequest(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "Bad Request", http.StatusBadRequest)
	fmt.Fprintf(w, "Usage:\nconservation/v1/country/countrycode\nconservation/v1/species/specieskey\nconservation/v1/diag")
}
