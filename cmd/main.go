package main

//  Importing necessary packages
import (
	"fmt"
	"log"
	"net/http"
	"os"
	"speciesAPI"
	"time"
)

func main() {

	//Keeps track of time at start
	speciesAPI.Uptime = time.Now()

	//Gets port and checks if it's "80", if not port is set to be "80"
	port := os.Getenv("PORT")
	if port == "" {
		port = "80"
		fmt.Println("$PORT not found! Setting to 80")
	}

	//HandleFunc() for each endpoint
	http.HandleFunc("/", speciesAPI.HandlerBadRequest)
	http.HandleFunc("/conservation/v1/species/", speciesAPI.HandlerSpecies)
	http.HandleFunc("/conservation/v1/country/", speciesAPI.HandlerOccurrence)
	http.HandleFunc("/conservation/v1/diag/", speciesAPI.HandlerDiag)

	//Prints which port server is listening on
	fmt.Println("Listening on port " + port)

	//Initialises server
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
