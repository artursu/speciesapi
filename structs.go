package speciesAPI

//Struct to retrieve data from RESTcountries
type country struct {
	Name    string `json:"name"`
	Capital string `json:"capital"`
	Flag    string `json:"flag"`
}

//Struct to retrieve data from GBIF endpoint species
type species struct {
	Key            int    `json:"key"`
	Kingdom        string `json:"kingdom"`
	Phylum         string `json:"phylum"`
	Order          string `json:"order"`
	Family         string `json:"family"`
	Genus          string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName  string `json:"canonicalName"`
	Year           string `json:"year"`
}

//Struct received from GBIF endpoint occurrence
type occurrences struct {
	Results []result `json:"results"`
}

//This is used to separate each single occurrence received from GBIF
type result struct {
	SpeciesKey     int    `json:"speciesKey"`
	AcceptedTaxonKey     int    `json:"acceptedTaxonKey"`
	ScientificName string `json:"scientificName"`
}

//Struct that contains combined data from RESTcountries and GBIF
type countryAndSpecies struct {
	Name       string   `json:"name"`
	Capital    string   `json:"capital"`
	Flag       string   `json:"flag"`
	Species    []string `json:"species"`
	SpeciesKey []int    `json:"speciesKey"`
}

//Struct that contains diagnostics data about API
type diagStruct struct {
	Gbif          string `json:"gbif"`
	Restcountries string `json:"restcountries"`
	Version       string `json:"version"`
	Uptime        string `json:"string"`
}
