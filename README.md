# Species Monitor

Cloud Assignment 1

Not every species has a speciesKey, in this case the API uses acceptedTaxonKey instead

For the sake of consistency, the API uses "scientificName" as "species", since not every occurrence entry has "species"
